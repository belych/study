import React, { Component } from "react";
import './styles/App.scss';
import EditTask from './components/EditTask.jsx';
import ListTask from "./components/ListTask.jsx";

class App extends Component {
    state = {
        tasks: [],
    }

    componentDidMount() {
        const localtasks = JSON.parse(localStorage.getItem('tasks'));
        if (localtasks) {
            this.setState({ tasks: localtasks });
        }
    }

    componentDidUpdate() {
        this._updateLocalStorage();
    }

    handleTaskAdd = (task) => {
        let newTasks = this.state.tasks.slice();
        newTasks.unshift(task);
        this.setState({ tasks: newTasks });
    }

    handleTaskDelete = (task) => {
        let taskId = task.target.dataset.taskid;
        let newTasks = this.state.tasks.filter((task) => Number(taskId) !== task.id);
        this.setState({ tasks: newTasks });
    }

    handleTaskDone = (task) => {   
        let taskId = task.currentTarget.dataset.taskid;
        this.state.tasks.map((task) => {
            if (Number(taskId) == task.id) {
                task.done = !task.done;
                this.setState({})
            }
        })
    }
    handleTaskText = (task)=> {
        console.log(task.target.dataset.taskid);
        // let taskId = task.target.dataset.taskid;
        // let newTasks = this.state.tasks.map((item)=>{
        //     if(taskId == item.id){
        //         item.text=1;
        //         console.log(task.target);
        //     }
        // })  
    }

    _updateLocalStorage = () => {
        let tasks = JSON.stringify(this.state.tasks);
        localStorage.setItem('tasks', tasks);
    }

    render() {
        return (
            <div className='to-do-list'>
                <EditTask
                    onTaskAdd={this.handleTaskAdd}
                    done={this.state.done}
                    style={this.state.style} />
                <ListTask data={this.state.tasks}
                    onTaskDelete={this.handleTaskDelete}
                    onTaskClick={this.handleTaskClick}
                    handleTaskDone={this.handleTaskDone}
                    handleTaskText={this.handleTaskText} />
            </div>
        )
    }
}

export default App;