import React , {Component} from "react";
import TextField from './TextField.jsx';
import Button from './Button.jsx';
import '../styles/EditTask.scss';

class EditTask extends Component{
    state={
        text:'',
    }

    handleTextChange = (task)=>{
        this.setState({text: task.target.value});
    }

    handleTaskAdd = ()=>{
        let newTask = {
            id: Date.now(),
            text: this.state.text,
            done: false
        };
        this.props.onTaskAdd(newTask);
        this.setState({ text: '' })
    }
  
    render(){
        return(
            <div className='edit-task'>
            <TextField 
                className='edit-task__text'
                value={this.state.text}
                onChange={this.handleTextChange}
                />
            <Button value='add' className='edit-task__button' onClick={this.handleTaskAdd}/>
            </div>
        )
    }
}

export default EditTask;