import React , {Component} from "react";

class TextField extends Component{
   
    render(){
        return(
            <textarea
                placeholder='Введите заметку'
                className={this.props.className}
                value={this.props.value}
                onChange={this.props.onChange}
                />
        )
    }
}

export default TextField;