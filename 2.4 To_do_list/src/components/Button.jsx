import React , {Component} from "react";
import '../styles/Button.scss';

class Button extends Component{
    render(){
        return(
            <button
                className={this.props.className}
                onClick={this.props.onClick}
                >{this.props.value}</button>
        )
    }
}

export default Button;