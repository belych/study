import React, { Component } from "react";
import '../styles/Task.scss';

class Task extends Component {
    //     state = {
    //         style: {
    //         background: { background: 'white'},
    //         check: { display: 'none' },
    //         textDecoration: {textDecoration: 'inherit'}
    //     }
    // }
    render() {
        return (
            <li className='task-list__item task-list-item'>
                <div className={this.props.done ? 'task-list-item__on task-list-item__on_visible':
                'task-list-item__on'}
                    onClick={this.props.handleTaskDone}
                    data-taskid={this.props.id}
                >
                    <div className={this.props.done ? 
                    'task-list-item__on-img task-list-item__on-img_visible':
                    'task-list-item__on-img'}>&#10003;</div>
                </div>
                <div className={this.props.done ? 
                'task-list-item__text task-list-item__text_visible':
                'task-list-item__text'}
                    onClick={this.props.handleTaskText}
                    data-taskid={this.props.id}
                    data-value={this.props.text}>{this.props.text}</div>
                <span className='task-list-item__del'
                    data-taskid={this.props.id}
                    onClick={this.props.onDelete}>х</span>
            </li>
        )
    }
}

export default Task;