import React, { Component } from "react";
import Task from './Task.jsx';

class ListTask extends Component {

    render() {
        return (
            <ul className='task-list'>
                {this.props.data.map((item) => {
                    return <Task key={Math.random() * 100}
                        id={item.id}
                        text={item.text}
                        done={item.done}
                        onDelete={this.props.onTaskDelete}
                        handleTaskText={this.props.handleTaskText}
                        handleTaskDone={this.props.handleTaskDone} />
                })}
            </ul>
        )
    }
}

export default ListTask;