class Hello extends React.Component{
    state = { 
        text: null,
  }
    
    handleText = (event)=>{
        this.setState({text: event.target.value})
    }

    render(){
        return (
            <div>
            <input type='text'className='add-text' onChange={this.handleText} placeholder='Введите текст'/> 
            {
                (this.state.text) ? <h1>Hello, {this.state.text}!</h1> : <h1>Hello, stranger!</h1>
            }
            </div>
        )
    }
}

const App = ()=>{
    return (
      <React.Fragment>
        <Hello/>
      </React.Fragment>
    )
  }

ReactDOM.render(
    <App/>,
    document.getElementById('root')
  );