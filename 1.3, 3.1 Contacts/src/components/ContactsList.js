import React, { Component } from "react";
import Contact from './Contact';

class ContactsList extends Component{
    state = {
        contacts_list: this.props.data
    }

    handleSearch = (event)=> {
        let searchQuery = event.target.value.toLowerCase();
        let contact_list_filter = this.props.data.filter((item)=>{
            let searchValue = item.name.toLowerCase();
            return  searchValue.indexOf(searchQuery) !== -1;
        })
        this.setState({contacts_list: contact_list_filter});
    }
    render(){
        return(
            <div className='contacts'>
                <input type='text' className='contacts__search-field' onChange={this.handleSearch}/>
                <ul className='contcts-list'>
                    {
                        this.state.contacts_list.map((item)=>{
                            return <Contact key={item.id} data={item} name={item.name} phoneNumber={item.phoneNumber} image={item.image} address={item.address} email={item.email}/>
                        })
                    }
                </ul>
            </div>
        )
    }
}

export default ContactsList;