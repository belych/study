import React, { Component } from "react";

class Contact extends Component{
    state = {
        visible: false,
    }

    handleRearMoreClick = ()=>{
        this.setState({visible: !this.state.visible});
    }

    render(){
        const {name, phoneNumber, image, address, email} = this.props.data;
        return(
            <li className='contact' onClick={this.handleRearMoreClick}>
                <img className='contact__img' src={image}/>
                <div className='contact__info'>
                    <div className='contact__name'>{name}</div>
                    <div className='contact__number'>{phoneNumber}</div>
                    {
                        (this.state.visible) ? <div className='contact__info-more'><div className='contact__address'>{address}</div><div className='contact__email'>{email}</div></div>: ''
                    }   
                </div>
            </li>
        )
    }
}

export default Contact;