import React from "react";
import data from '../../public/data/data';
import ContactsList from './ContactsList';
import '../styles/index.css';

const App = ()=>{
    return (
      <React.Fragment>
        <ContactsList data={data}/>
      </React.Fragment>
    )
  }

  export default App;