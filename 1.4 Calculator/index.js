
class CalcBtn extends React.Component{
    render(){
        return(
            <div className='parent'>
                <button
                    className= 'button parent__button' onClick={this.props.onClick}>
                    <span className= 'button__text'>{this.props.name}</span>
                </button>
            </div>
        )
    }
}


class CalcInput extends React.Component{
    state = {
        number: 0,
    }

    render(){
        return(
            <input 
                type='number' 
                className='calc__number' 
                placeholder='Введите число'
                onChange={this.props.onChange}
                />
        )
    }
}


class Calculator extends React.Component{
    state = {
        number1: 0,
        number2: 0,
        res: 0,
    }

    handleNumber1 = (event)=>{
        this.setState({number1: event.target.value});
    }
    handleNumber2 = (event)=>{
        this.setState({number2: event.target.value});
    }
    sum = ()=>{
        this.setState({res: Number(this.state.number1) + Number(this.state.number2)}) ;
    }
    sub = ()=>{
        this.setState({res: Number(this.state.number1) - Number(this.state.number2)}) ;
    }
    mul = ()=>{
        this.setState({res: Number(this.state.number1) * Number(this.state.number2)}) ;
    }

    render(){
        return (
            <div className='calc'>
                <CalcInput onChange={this.handleNumber1}/>
                <CalcInput onChange={this.handleNumber2}/>
                <div className='container-btn'>
                <CalcBtn name={'+'} onClick={this.sum}/>
                <CalcBtn name={'-'} onClick={this.sub}/>
                <CalcBtn name={'*'} onClick={this.mul}/>
                </div>
                <div>Oтвет: {this.state.res}</div>
            </div>
        )
    }
};


const App = ()=>{
    return (
      <React.Fragment>
        <Calculator/>
      </React.Fragment>
    )
  }

ReactDOM.render(
    <App/>,
    document.getElementById('root')
  );