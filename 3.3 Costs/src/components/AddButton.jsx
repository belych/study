import React from "react";
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';


export const AddButton = (props) => {
    const { classes, onClick } = props;
    return (
        <Tooltip title="Add">
            <Fab
                color="primary"
                aria-label="Add"
                className={classes.fab}
                onClick={onClick}>
                <AddIcon />
            </Fab>
        </Tooltip>
    )
}
