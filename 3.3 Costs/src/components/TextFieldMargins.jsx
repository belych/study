import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import { AddButton } from "./AddButton.jsx";
import RadioButtons from './RadioSelect.jsx'
import { TYPES } from '../common/constants.js';

const styles = theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
  },
  fab: {
    margin: theme.spacing.unit,
  }
});

class TextFieldMargins extends Component {

  state = {
    type: TYPES.incoming,
    amount: 0
  }

  changeType = evt => {
    this.setState({ type: evt.target.value })
  }

  changeAmount = evt => {
    const amount = evt.target.value;
    if (Number.isNaN(Number(amount)))
      return;
    this.setState({ amount })
  }

  addData = () => {
    let { type, amount } = this.state;
    amount = type === TYPES.costs ? Number(amount * -1) : Number(amount);
    this.props.addData(type, amount)
  }

  render() {
    return (
      <div className={this.props.classes.container}>
        <RadioButtons type={this.state.type} onChange={this.changeType} />
        <TextField
          label="Число"
          id="margin-none"
          value={this.state.amount}
          onChange={this.changeAmount}
          className={this.props.classes.textField}
          helperText="Введите доход или расход"
        />
        <AddButton classes={styles} onClick={this.addData} />

      </div>

    );
  }
};

TextFieldMargins.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(TextFieldMargins);