import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import green from '@material-ui/core/colors/green';
import Radio from '@material-ui/core/Radio';
import { TYPES } from '../common/constants';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const styles = {
    root: {
        color: green[600],
        '&$checked': {
            color: green[500],
        },
    },
    checked: {},
};

class RadioButtons extends React.Component {
    render() {

        return (
            <div>
                 <FormControlLabel value="female" control={<Radio
                    checked={this.props.type === TYPES.incoming}
                    onChange={this.props.onChange}
                    value={TYPES.incoming}
                    name="radio-incoming-type"
                    aria-label="Доходы"
                />} label="Доходы" />
                
                <FormControlLabel value="male" control={<Radio
                    checked={this.props.type === TYPES.costs}
                    onChange={this.props.onChange}
                    value={TYPES.costs}
                    name="radio-cost-type"
                    aria-label="Расходы"
                />} label="Расходы" />
            </div>
        );
    }
}

RadioButtons.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(RadioButtons);