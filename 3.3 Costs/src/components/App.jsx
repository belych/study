import React, { Component } from "react";
import TableCosts from './TableCosts.jsx';
import TextFieldMargins from './TextFieldMargins.jsx';

class App extends Component {

  state = {
    data: [],
  }

  addData = (type, amount) => {
    let data = this.state.data.slice();
    data.push({ type, amount });
    this.setState({ data })
  }

  render() {
    return (
      <div>
        <TextFieldMargins addData={this.addData}/>
        <TableCosts data={this.state.data} />
      </div>
    )
  }
}

export default App;
