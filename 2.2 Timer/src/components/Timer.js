import React, { Component } from "react";
import BtnTimer from "./Btn";


class Timer extends Component {
    state = {
        seconds: 0,
        pause: 0,
        show: false
    }

    componentDidMount() {
        this.timer = setInterval(this.tick, 1000);
    }

    tick = () => {
        this.setState({ seconds: this.state.seconds + 1 })
    }

    startTimer = () => {
        this.timer = setInterval(this.tick, 1000);
        this.setState({ show: false });
    }

    pauseTimer = () => {
        this.setState({ seconds: this.state.seconds, show: true });
        clearInterval(this.timer);
    }
    resetTimer = () => {
        this.setState({ seconds: this.state.pause });
    }
    componentWillUnmount() {
        clearInterval(this.timer)
    }

    render() {
        return (
            <div className='timer-container'>
                <div>Прошло: {this.state.seconds}</div>
                <div>
                    {this.state.show ?
                        <BtnTimer value='Старт'
                            className='button'
                            onClick={this.startTimer} /> :
                        <BtnTimer value='Пауза' className='button'
                            onClick={this.pauseTimer} />}
                            
                    <BtnTimer value='Возобновить' className='button'
                        onClick={this.resetTimer} />
                </div>
            </div>
        )
    }
}

export default Timer; 