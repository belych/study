import React, { Component } from "react";

class BtnTimer extends Component {
    render() {
        return (
            <button
            className={this.props.className}
            value={this.props.value}
            onClick={this.props.onClick}
            >{this.props.value}</button>
        )
    }
}

export default BtnTimer;