import React, { Component } from "react";
import '../styles/index.css';

class Note extends Component {

    render() {
        let style = { backgroundColor: this.props.color };
        return (
            <div className='note' style={style} onClick={this.props.onClick}>
                <div className='note__delete'
                    data-noteid={this.props.noteId}
                    onClick={this.props.onDelete}>x</div>
                {this.props.children}
            </div>
        )
    }
}

export default Note;