import React, { Component } from "react";
import Note from './Note.js';
import '../styles/index.css';
import Masonry from "masonry-layout";

class NotesGrid extends Component {
    state = {
        filter_text: this.props.filter_text
    }

    componentDidMount() {
        let grid = this.refs.grid;
        this.msnry = new Masonry(grid, {
            itemSelector: '.note',
            columnWidth: 200,
            gutter: 10 // расстояние между колонками
        });
    }
    componentDidUpdate(prevProps) {
        if (this.props.notes.length !== prevProps.notes.length) {
            this.msnry.reloadItems();
            this.msnry.layout();
        }
        if (this.props.filter_text !== prevProps.filter_text) {
            this.msnry.reloadItems();
            this.msnry.layout();
        }
    }

    editNote = (e) => {
        console.log('g')
    }

    renderNotes = () => {
        return this.props.notes
            .filter((item) => {
                if (!this.props.filter_text || !this.props.filter_text.toLowerCase)
                    return true;
                let searchValue = item.text.toLowerCase();
                return searchValue.indexOf(this.props.filter_text.toLowerCase()) !== -1;
            })
            .map((item) => {
                return <Note
                    key={item.id + item.text}
                    color={item.color}
                    noteId={item.id}
                    onDelete={this.props.onNoteDelete}
                    onClick={this.editNote}
                >{item.text}</Note>
            })
    }

    render() {
        return (
            <div className='notes-grid' ref='grid'>
                {this.renderNotes()}
            </div>
        )
    }
}

export default NotesGrid;