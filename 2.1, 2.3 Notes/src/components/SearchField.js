import React, { Component } from "react";

class SearchField extends Component{
    render(){
        return(
            <input
                className='note-editor__search'
                type='text'
                onChange={this.props.onChange}
                />
        )
    }
}

export default SearchField;