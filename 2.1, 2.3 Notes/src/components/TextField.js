import React, { Component } from "react";

class TextField extends Component{
    render(){
        return(
            <textarea
            placeholder={this.props.placeholder}
            className={this.props.className}
            value={this.props.value}
            onChange={this.props.onChange}
            rows={this.props.rows} />
        )
    }
}

export default TextField;