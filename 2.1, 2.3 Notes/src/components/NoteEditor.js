import React, { Component } from "react";
import '../styles/index.css';
import { ColorSquere } from './ColorSquere';
import SearchField from './SearchField';
import Btn from './Btn';
import TextField from './TextField';

class NoteEditor extends Component {
    state = {
        text: '',
        filter: ''
    }
    handleTextChange = (e) => {
        this.setState({ text: e.target.value })
    }
    handleFilterChange = (e) => {
        this.setState({ filter: e.target.value })
    }
    handleNoteAdd = () => {
        let newNote = {
            id: Date.now(),
            text: this.state.text,
        };
        this.props.onNoteAdd(newNote);
        this.setState({ text: '' })
    }
    handleSearch = () => {
        this.props.handleSearch(this.state.filter);
    }
    
    render() {
        let color = ['rgb(206, 57, 37)', 'rgb(65, 149, 189)',
         'rgb(120, 189, 41)', 'rgb(224, 226, 83)', 'rgb(100, 226, 83)'];
        return (
            <div className='note-editor'>
                <SearchField onChange={this.handleFilterChange} />
                <Btn className='button'
                    onClick={this.handleSearch}
                    value='search' />
                <TextField
                    placeholder='Enter your note here...'
                    className='note-editor__text'
                    value={this.state.text}
                    onChange={this.handleTextChange}
                    rows={5} />
                <div className='note-editor__control'>
                    <div className='note-editor__colors'>
                        {color.map((item) => [
                            <ColorSquere
                                key={item}
                                item={item}
                                onClick={this.props.handleColor}
                            />
                        ])}
                    </div>
                    <Btn
                        className='button'
                        onClick={this.handleNoteAdd}
                        value='Add' />
                </div>
            </div>
        )
    }
}

export default NoteEditor;