import React from 'react';

export class ColorSquere extends React.Component {

    handleClick = () => {
        this.props.onClick(this.props.item)
    }

    render() {
        return <div
            className='note-editor__color'
            style={{ backgroundColor: this.props.item }}
            onClick={this.handleClick}></div>
    }
}