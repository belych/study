import React, { Component } from "react";

class Btn extends Component {
    render() {
        return (
            <button
                className={this.props.className}
                onClick={this.props.onClick}
                value={this.props.value}>
                {this.props.value}</button>
        )
    }
}

export default Btn;