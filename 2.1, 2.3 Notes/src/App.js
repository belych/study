import React from "react";
import { Component } from 'react';
import NoteEditor from './components/NoteEditor.js';
import NotesGrid from './components/NotesGrid.js';
import './styles/index.css';

class App extends Component {
    state = {
        notes: [],
        color: '',
        search: '',
    }

    componentDidMount() {
        let localNotes = JSON.parse(localStorage.getItem('notes'));
        if (localNotes) {
            this.setState({ notes: localNotes });
        }
    }

    componentDidUpdate() {
        this._updateLocalStorage();
    }

    handleNoteDelete = (note) => {
        const noteid = note.target.dataset.noteid;
        console.log(noteid);
        var newNotes = this.state.notes.filter((note) => note.id !== Number(noteid));
        this.setState({ notes: newNotes });
    }

    handleNoteAdd = (newNote) => {
        newNote.color = this.state.color;
        let newNotes = this.state.notes.slice();
        newNotes.unshift(newNote);
        this.setState({ notes: newNotes });
    }

    handleColor = (value) => {
        this.setState({ color: value })
    }

    handleSearch = (value) => {
        this.setState({ search: value })
    }

    _updateLocalStorage() {
        let notes = JSON.stringify(this.state.notes);
        localStorage.setItem('notes', notes);
    }

    render() {
        return (
            <div className='notes'>
                <div className='notes__header'>Notes</div>
                <NoteEditor
                    onNoteAdd={this.handleNoteAdd}
                    handleColor={this.handleColor}
                    handleSearch={this.handleSearch}
                    handleNoteSearch={this.handleNoteSearch} />
                <NotesGrid
                    notes={this.state.notes}
                    onNoteDelete={this.handleNoteDelete}
                    filter_text={this.state.search} />
            </div>
        )
    }
    
}
export default App;