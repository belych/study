import React, { Component } from "react";
import Article from './Article';

class ArticlesList extends Component {
    render(){
        const {data} = this.props;
      return (
        <div className='articles'>
      <ul className='articles-list'>
        {
            data.map((item)=>{
                return <Article key={item.id} 
                                data={item} 
                                title={item.title} 
                                author={item.author} 
                                text={item.text}/>
            })
        }
      </ul>
      </div>
      )
    }
  }

  export default ArticlesList;