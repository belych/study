import React, { Component } from "react";

class Article extends Component {
    render(){
        const {title, author, text} = this.props.data;
        return (
            <li className='article'>
              <div className='article__title'>{title}</div>
              <div className='article__author'>{author}</div>
              <div className='article__ text'>{text}</div>
            </li>
          )
    }
  }

export default Article;