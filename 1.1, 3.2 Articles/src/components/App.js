import React from 'react';
import data from '../../public/data/data';
import ArticlesList from './ArticlesList';
import '../styles/index.css';

const App = ()=>{
    return (
      <React.Fragment>
        <ArticlesList data={data}/>
      </React.Fragment>
    )
  }

export default App;